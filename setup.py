"""A setuptools based setup module for configmount.
"""

from setuptools import setup, find_packages, Command
from os import path, system, stat, listdir
from io import open
import stat as _stat

fwd = path.abspath(path.dirname(__file__))

def find_description():
  # Get the long description from the README file
  rf = path.join(fwd, 'README.md')
  if path.isfile(rf):
    with open(rf, encoding='utf-8') as f:
        return f.read()
  else:
    return None

def find_scripts(location="./scripts"):
  result = []
  loc = path.join(fwd, location)
  executable = _stat.S_IEXEC | _stat.S_IXGRP | _stat.S_IXOTH
  if path.isdir(loc):
    for filename in [path.join(loc, p) for p in listdir(loc)]:
        if path.isfile(filename):
            st = stat(filename)
            mode = st.st_mode
            if mode & executable:
              result.append(filename)
  return result
            
            
class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        system('rm -vrf ./dist ./*.pyc ./*.tgz ./*.egg-info')

# TODO
setup(
    name='argdirective', 
    version='0.0.3',
    description='@description@', 
    long_description=find_description(),
    long_description_content_type='text/markdown',
    url='https://www.mail.org',
    author='domson',
    author_email='no@mail.org',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: User Interfaces',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='@keywords@',
    packages=find_packages(),
    scripts=find_scripts(),
    python_requires='>=3',
    project_urls={
        'Bug Reports': 'https://bugs.mail.org',
        'Source': 'https://sources.mail.org',
    },
)
